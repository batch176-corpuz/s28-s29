/* 
https://boodle.zuitt.co/notes/1259
*** GOAL ***
    create server-side app using express web framework
    *** PROCESS ***
        - append entire app to npm (npm init -y)

        - express == used as the main component to create server
            - require() == directive used to get library/component needed inside module
            - import with require()

        - prepare environment where project will be served (nodemon)
            - package.json > add new script > value: "nodemon index.js"

        - prepare remote repo (git)
            - ***always*** disable the node_modules folder
            - reasons: 
                - crowds repository
                - harder to commit changes
                - if project is deployed in platforms (heroku, netlify, vercel, etc.), project will autoreject as node_modules isn't recognized
            - create .gitignore folder
            - include /node_modules in .gitignore
*/

/* 
*** package.json ***
    why does "npm start" work without "run" keyword in scripts?
        - "start" is globally recognized amongst node projects and frameworks as the 'default' command strip to execute a task/project
    for unconventional scripts, append "run" after "npm"
*/

/* 
*** nodemon ***
Create a runtime environment that autofixes all changes in app
    - we'll use a utility called nodemon
    - nodemon provides autofixing of changes
    - upon starting the entry point module with nodemon
        - you will be able to 'append' the application with the proper - runtime environment
        - saves time and effort upon committing changes to app
*/

console.log('welcome to our express API server');
console.log(`───────────────────────────────────────
───▐▀▄───────▄▀▌───▄▄▄▄▄▄▄─────────────
───▌▒▒▀▄▄▄▄▄▀▒▒▐▄▀▀▒██▒██▒▀▀▄──────────
──▐▒▒▒▒▀▒▀▒▀▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▀▄────────
──▌▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▄▒▒▒▒▒▒▒▒▒▒▒▒▀▄──────
▀█▒▒▒█▌▒▒█▒▒▐█▒▒▒▀▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▌─────
▀▌▒▒▒▒▒▒▀▒▀▒▒▒▒▒▒▀▀▒▒▒▒▒▒▒▒▒▒▒▒▒▒▐───▄▄
▐▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▌▄█▒█
▐▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒█▒█▀─
▐▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒█▀───
▐▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▌────
─▌▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▐─────
─▐▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▌─────
──▌▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▐──────
──▐▄▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▄▌──────
────▀▄▄▀▀▀▀▀▄▄▀▀▀▀▀▀▀▄▄▀▀▀▀▀▄▄▀────────
`);

//import express utilities
const express = require("express");